require('dotenv').config();
const Discord = require('discord.js');
const client = new Discord.Client();
const TOKEN = process.env.TOKEN;
const axios = require('axios');

const baseUrl = 'https://corona.lmao.ninja/v2';

client.login(TOKEN);

client.on('ready', () => {
  console.info(`Logged in as ${client.user.tag}!`);
});

client.on('message', (payload) => {
  const {content, channel, author} = payload;
  if (content.substring(0, 1) == '!') {
    var args = content.substring(1).split(' ');
    var cmd = args[0];
   
    args = args.splice(1);
    switch(cmd) {
        // !ping
        case 'sem-is-homo':
          channel.send('Godverdomme maar echt');
          break;
        case 'status':
            let country = args[0];
            if(!country) {
              country = 'netherlands'
            }

            axios.get(`${baseUrl}/${country}`).then((res) => {
              const data = res.data;
              if(res.data.cases) { 
                const embed = {
                  title: `${country} cases`,
                  fields: [
                    { name: 'Total cases', value: data.cases },
                    { name: 'Today cases', value: data.todayCases },
                    { name: 'Recovered (!)', value: data.recovered },
                    { name: 'Deaths', value: data.deaths },
                    { name: 'Today deaths', value: data.todayDeaths },
                    { name: 'Critical', value: data.critical }
                  ]
                };
                channel.send({embed: embed});
              } else {
                channel.send('Country not found.');
              }
            }).catch((err) => {
              channel.send('Something went wrong. I have an error');
              console.log(err);
            });
        break;
        case 'countries':
          axios.get(`${baseUrl}/countries`).then((res) => {
            const data = res.data;
            let response = '';
            let formattedData = [];
            channel.send('possible countries:');


            data.forEach((country) => {
              formattedData.push(country.country);
            });

            console.dir(formattedData.sort(), {'maxArrayLength': null});
            console.log(formattedData.length);

            formattedData.forEach((country, i) => {
              if(i % 40 == 0) {
                response = response + ' ' + country + ',';
                console.log('reached 40 once, sending countries 1-40')
                console.log(i);
                channel.send(response.slice(0, -1));
                response = '';
              } else {
                response = response + ' ' + country + ',';
                console.log('adding country!');
              }
            });
            if(response) {
              channel.send(response.slice(0, -1));
            }
          }).catch((err) => {
            channel.send('Something went wrong. I have an error');
            console.log(err);
          });
        break;
        case 'all':
          axios.get(`${baseUrl}/all`).then((res) => {
            const data = res.data;
              if(res.data.cases) { 
                const embed = {
                  title: `Worldwide stats`,
                  fields: [
                    { name: 'Total cases', value: data.cases },
                    { name: 'Total deaths', value: data.deaths },
                    { name: 'Total recovered (!)', value: data.recovered }
                  ]
                };
                channel.send({embed: embed});
              } else {
                channel.send('The bot encountered an error');
              }}).catch((err) => {
                channel.send('Something went wrong. I have an error');
                console.log(err);
              });
        break;
        case 'help' || 'halp':
          const embed = {
            title: `Corona bot help`,
            description: 'This is the help menu of the corona bot. Below listed are all the possible comments. The data is fetched from an API outside of my reach. The data MAY not be up to date.',
            fields: [
              { name: '!status <country>', value: 'Gets the current stats of the given country.' },
              { name: '!all', value: 'Gets the total world stats.' },
              { name: '!countries', value: 'Lists all the possible countries that are able to be searched on.' },
              { name: '!help', value: 'Lists this help menu.' }
            ]
          };
          channel.send({embed: embed});
        break;
     }
 }
});
